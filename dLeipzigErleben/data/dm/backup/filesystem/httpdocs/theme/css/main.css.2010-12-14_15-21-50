html, body {
    height: 100%;
}
#dm_page {
  min-height: 100%;
  height: auto !important;
  background-color: #FFF;
  padding: 0 16px;
  width: 924px;
  margin: 0 auto;
  overflow: hidden;
}
body {
    color: #333333;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    margin: 0;
}
h1 {
    font-size: 1.6em;
}
h2 {
    font-size: 1.4em;
}
h3 {
    font-size: 1.3em;
    font-weight: bold;
}
h4 {
    font-size: 1.2em;
    font-weight: bold;
}
h1, h2, h3, h4 {
    color: #f60d00;
    margin-bottom: 0.5em;
}
a {
    color: #333333;
    text-decoration: none;
}
a:hover {
    color: #000000;
}
h1 a, h2 a, h3 a, h4 a,
h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover {
  color: #f60d00;
}
p {
  line-height: 1.5em;
}

/* common classes */
.small {
  font-size: 0.9em;
}
.big {
  font-size: 1.2em;
}
.black {
  color: #000000;
}
.red {
  color: #f60d00;
}
.bold {
  font-weight: bold;
}
.display_none {
  display: none;
}

.search_form {
    text-align: right;
}
.search_form input.query {
    margin-right: 10px;
}


.divider {
  border-bottom: 1px solid #f60d00;
  height: 0;
  line-height: 0;
  margin: 30px 0;
}

.dm_layout_left .dm_widget {
  background-color: #ffffff;
  padding-bottom: 10px;
}

.dm_layout_left .dm_widget.last,
.dm_layout_left .dm_widget:last-child {
  padding-bottom: 0;
}

.layout-left-bottom {
  background: transparent url('../../uploads/assets/bg-layout-left-bottom.gif') no-repeat left top scroll;
  height: 8px;
  width: 180px;
}

.link.teaser-tickets,
.link.teaser-stadtplan,
.link.teaser-schenken {
  background: transparent none no-repeat left top scroll;
  display: block;
  font-size: 14px;
  font-weight: bold;
  height: 22px;
  line-height: 22px;
  padding-top: 1px;
  padding-left: 3px;
  padding-right: 4px;
  text-align: center;
  width: 173px;
}

.link.teaser-tickets {
  background-image: url('../../uploads/assets/teaser-tickets-bestellen.gif');
  padding-bottom: 81px;
}
.link.teaser-stadtplan {
  background-image: url('../../uploads/assets/teaser-stadtplan.jpg');
  padding-bottom: 192px;
}
.link.teaser-schenken {
  background-image: url('../../uploads/assets/teaser-leipzig-schenken.gif');
  padding-bottom: 81px;
}

.teaser-hotline {
  background: transparent url('../../uploads/assets/teaser-hotline.gif') no-repeat left top scroll;
  color: #ffffff;
  font-size: 15px;
  font-weight: bold;
  height: 44px;
  line-height: 22px;
  padding: 2px 4px 4px 3px;
  text-align: center;
  width: 173px;
}

.tour_date_list_by_date .elements .element,
.tour_list_handicap .elements .element {
  list-style-type: none;
  padding: 0.5em 0;
}
.price-table table {
  border-collapse: collapse;
  font-weight: bold;
  width: 100%;
}
.price-table table th.col-german,
.price-table table th.col-foreign {
  width: 30%;
}
.price-table table thead tr th,
.price-table table tbody tr.even td {
  background-color: #eeeeee;
}
.price-table table th,
.price-table table td {
  padding: 2px;
}
.price-table table .col-hours {
  padding-left: 5px;
}
.price-table table .col-german,
.price-table table .col-foreign {
  text-align: center;
}
.other-price-table table {
  margin-bottom: 10px;
}
.other-price-table table td {
  padding: 2px 30px 2px 0;
}

.tour_show .markdown {
  padding-right: 200px;
}

.dm_widget.content_text .dm_widget_inner.image-right .text_content .text_image {
  float: right;
  margin: 0 0 15px 15px;
}


.main_sitemap ul li {
  line-height: 1.5em;
}
.main_sitemap ul li ul {
  padding-left: 1em;
}

.dm_layout.culture_en .hide_english {
  display: none;
}


.dm_contact_form form .row{
  margin-bottom: 5px;
}
.dm_contact_form form label {
  clear: left;
  float: left;
  width: 10em;
}
.dm_contact_form form input,
.dm_contact_form form textarea,
.dm_contact_form form ul.error_list {
  float: left;
}
.dm_contact_form form .submit_wrap {
  padding-left: 10em;
}
.dm_contact_form form label,
.dm_contact_form form input,
.dm_contact_form form ul.error_list {
  height: 1.5em;
  line-height: 1.5em;
}
.dm_contact_form form ul.error_list {
  color: red;
  padding-left: 5px;
}
.dm_contact_form #dm_contact_form_name,
.dm_contact_form #dm_contact_form_email {
  width: 200px;
}
.dm_contact_form textarea {
  height: 150px;
  width: 400px;
}

.dm_contact_form input.submit {
  padding: 2px 5px;
}
