<?php

require_once '/var/www/html/leipzig-erleben.com/dLeipzigErleben/diem/dmCorePlugin/lib/core/dm.php';
dm::start();

class ProjectConfiguration extends dmProjectConfiguration
{

  public function setup()
  {
    parent::setup();
    
    $this->enablePlugins(array(
      // add plugins you want to enable here
      'dmCkEditorPlugin',
      'dmContactPlugin',
    ));

    $this->setWebDir(sfConfig::get('sf_root_dir').'/../web');

    ini_set('session.save_path', sfConfig::get('sf_root_dir').'/php_sessions');
  }
  
}
