<?php

class leCsv2ymlTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    // // add your own options here
    // $this->addOptions(array(
    //   new sfCommandOption('my_option', null, sfCommandOption::PARAMETER_REQUIRED, 'My option'),
    // ));

    $this->namespace        = 'le';
    $this->name             = 'csv2yml';
    $this->briefDescription = 'Converts CSV to YML file (as a preparation for import)';
    $this->detailedDescription = <<<EOF
The [le:csv2yml|INFO] task does things.
Call it with:

  [php symfony le:csv2yml|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // add your code here
  }
}
