<?php

class leImportcsvTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'le';
    $this->name             = 'import-csv';
    $this->briefDescription = 'Import CSV to database';
    $this->detailedDescription = <<<EOF
The [le:import-csv|INFO] task does things.
Call it with:

  [php symfony le:import-csv|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // statistic vars
    $numNewTourDates = 0;

    // tour cache array to prevent making too much database queries
    $tours = array();

    // iterate all csv files in data/csv/ folder
    foreach (sfFinder::type('file')->name('*.csv')->in(sfConfig::get('sf_data_dir').'/csv/') as $file)
    {
        // read contents of file
        $lines = file($file, FILE_IGNORE_NEW_LINES);
        if ($lines !== false)
        {
            $this->logSection('file', sprintf('Processing file %s', $file));
            foreach ($lines as $line_number => $line)
            {
                $fields = explode(';', $line);

                // get tour by id
                $id = intval($fields[7]);
                if ($id > 0)
                {
                    if (!array_key_exists($id, $tours))
                    {
                        $tours[$id] = Doctrine::getTable('Tour')->findOneById($id);
                    }
                    $tour = $tours[$id];
                    if (false === $tour){
                        $this->logSection('problem', sprintf('Tour with ID "%s" not found.', $id));
                    }
                    else {
                        // get day
                        $date_day = preg_replace('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', '$3-$2-$1', $fields[0]);

                        // get time
                        $date_time = preg_replace('/\'([0-9]{1,2}):([0-9]{1,2}) ab.*\'/', '$1:$2:00', $fields[4]);

                        $date = new TourDate();
                        $date->setTour($tour);
                        $date->setDateDay($date_day);
                        $date->setDateTime($date_time);
                        $date->save();

                        $numNewTourDates++;

                        $this->logSection('date+', sprintf('New Tour date created with ID: %s (Tour "%s" am %s um %s)', $date->getId(), $tour->getName(), $date_day, $date_time));
                    }
                }

            }
        }
    }

    $this->logBlock(sprintf('Import completed - %s tour dates added', $numNewTourDates), 'INFO');
  }
}