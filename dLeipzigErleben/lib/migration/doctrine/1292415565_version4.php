<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version4 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('dm_contact', 'contact_type', 'enum', '', array(
             'values' => 
             array(
              0 => 'simple',
              1 => 'group',
             ),
             ));
        $this->addColumn('dm_contact', 'company', 'string', '255', array(
             ));
        $this->addColumn('dm_contact', 'phone', 'string', '255', array(
             'notnull' => '1',
             ));
        $this->addColumn('dm_contact', 'street', 'string', '255', array(
             'notnull' => '1',
             ));
        $this->addColumn('dm_contact', 'zip', 'string', '255', array(
             'notnull' => '1',
             ));
        $this->addColumn('dm_contact', 'city', 'string', '255', array(
             'notnull' => '1',
             ));
    }

    public function down()
    {
        $this->removeColumn('dm_contact', 'contact_type');
        $this->removeColumn('dm_contact', 'company');
        $this->removeColumn('dm_contact', 'phone');
        $this->removeColumn('dm_contact', 'street');
        $this->removeColumn('dm_contact', 'zip');
        $this->removeColumn('dm_contact', 'city');
    }
}