<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version1 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('tour_translation', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'primary' => '1',
             ),
             'name' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '200',
             ),
             'description' => 
             array(
              'type' => 'clob',
              'extra' => 'ckeditor',
              'length' => '',
             ),
             'is_active' => 
             array(
              'type' => 'boolean',
              'notnull' => '1',
              'default' => '1',
              'length' => '25',
             ),
             'lang' => 
             array(
              'fixed' => '1',
              'primary' => '1',
              'type' => 'string',
              'length' => '2',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
              1 => 'lang',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->removeColumn('tour', 'name');
        $this->removeColumn('tour', 'description');
        $this->removeColumn('tour', 'is_active');
        $this->removeColumn('tour', 'created_at');
        $this->removeColumn('tour', 'updated_at');
        $this->addColumn('tour', 'position', 'integer', '8', array(
             ));
    }

    public function down()
    {
        $this->dropTable('tour_translation');
        $this->addColumn('tour', 'name', 'string', '200', array(
             'notnull' => '1',
             ));
        $this->addColumn('tour', 'description', 'clob', '', array(
             'extra' => 'ckeditor',
             ));
        $this->addColumn('tour', 'is_active', 'boolean', '25', array(
             'notnull' => '1',
             'default' => '1',
             ));
        $this->addColumn('tour', 'created_at', 'timestamp', '25', array(
             'notnull' => '1',
             ));
        $this->addColumn('tour', 'updated_at', 'timestamp', '25', array(
             'notnull' => '1',
             ));
        $this->removeColumn('tour', 'position');
    }
}