<?php

/**
 * Partner filter form base class.
 *
 * @package    leipzig-erleben
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePartnerFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'website'     => new sfWidgetFormDmFilterInput(),
      'image'       => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'position'    => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'website'     => new sfValidatorPass(array('required' => false)),
      'image'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'position'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    
    $this->mergeI18nForm();


    $this->widgetSchema->setNameFormat('partner_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Partner';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'website'     => 'Text',
      'image'       => 'ForeignKey',
      'position'    => 'Number',
      'id'          => 'Number',
      'name'        => 'Text',
      'description' => 'Text',
      'is_active'   => 'Boolean',
      'lang'        => 'Text',
      'created_at'  => 'Date',
      'updated_at'  => 'Date',
    );
  }
}
