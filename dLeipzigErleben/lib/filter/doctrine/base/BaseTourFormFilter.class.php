<?php

/**
 * Tour filter form base class.
 *
 * @package    leipzig-erleben
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTourFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_walkabout'              => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'is_excursion'              => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'meeting_point'             => new sfWidgetFormDmFilterInput(),
      'duration'                  => new sfWidgetFormDmFilterInput(),
      'price'                     => new sfWidgetFormDmFilterInput(),
      'image'                     => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'is_handicapped_accessible' => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'position'                  => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'is_walkabout'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_excursion'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'meeting_point'             => new sfValidatorPass(array('required' => false)),
      'duration'                  => new sfValidatorPass(array('required' => false)),
      'price'                     => new sfValidatorPass(array('required' => false)),
      'image'                     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'is_handicapped_accessible' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'position'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    
    $this->mergeI18nForm();


    $this->widgetSchema->setNameFormat('tour_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Tour';
  }

  public function getFields()
  {
    return array(
      'id'                        => 'Number',
      'is_walkabout'              => 'Boolean',
      'is_excursion'              => 'Boolean',
      'meeting_point'             => 'Text',
      'duration'                  => 'Text',
      'price'                     => 'Text',
      'image'                     => 'ForeignKey',
      'is_handicapped_accessible' => 'Boolean',
      'position'                  => 'Number',
      'id'                        => 'Number',
      'name'                      => 'Text',
      'description'               => 'Text',
      'is_active'                 => 'Boolean',
      'is_online_orderable'       => 'Boolean',
      'lang'                      => 'Text',
      'created_at'                => 'Date',
      'updated_at'                => 'Date',
    );
  }
}
