<?php

/**
 * DmContact filter form base class.
 *
 * @package    leipzig-erleben
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDmContactFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'         => new sfWidgetFormDmFilterInput(),
      'email'        => new sfWidgetFormDmFilterInput(),
      'body'         => new sfWidgetFormDmFilterInput(),
      'contact_type' => new sfWidgetFormChoice(array('choices' => array('' => '', 'simple' => 'simple', 'group' => 'group'))),
      'company'      => new sfWidgetFormDmFilterInput(),
      'first_name'   => new sfWidgetFormDmFilterInput(),
      'phone'        => new sfWidgetFormDmFilterInput(),
      'street'       => new sfWidgetFormDmFilterInput(),
      'zip'          => new sfWidgetFormDmFilterInput(),
      'city'         => new sfWidgetFormDmFilterInput(),
      'created_at'   => new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))),
      'updated_at'   => new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))),
    ));

    $this->setValidators(array(
      'name'         => new sfValidatorPass(array('required' => false)),
      'email'        => new sfValidatorPass(array('required' => false)),
      'body'         => new sfValidatorPass(array('required' => false)),
      'contact_type' => new sfValidatorChoice(array('required' => false, 'choices' => array('simple' => 'simple', 'group' => 'group'))),
      'company'      => new sfValidatorPass(array('required' => false)),
      'first_name'   => new sfValidatorPass(array('required' => false)),
      'phone'        => new sfValidatorPass(array('required' => false)),
      'street'       => new sfValidatorPass(array('required' => false)),
      'zip'          => new sfValidatorPass(array('required' => false)),
      'city'         => new sfValidatorPass(array('required' => false)),
      'created_at'   => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['created_at']->getOption('choices')))),
      'updated_at'   => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['updated_at']->getOption('choices')))),
    ));
    

    $this->widgetSchema->setNameFormat('dm_contact_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'DmContact';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'name'         => 'Text',
      'email'        => 'Text',
      'body'         => 'Text',
      'contact_type' => 'Enum',
      'company'      => 'Text',
      'first_name'   => 'Text',
      'phone'        => 'Text',
      'street'       => 'Text',
      'zip'          => 'Text',
      'city'         => 'Text',
      'created_at'   => 'Date',
      'updated_at'   => 'Date',
    );
  }
}
