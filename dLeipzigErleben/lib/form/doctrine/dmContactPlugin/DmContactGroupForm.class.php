<?php

/**
 * DmContact group form.
 *
 * @package    leipzig-erleben
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
class DmContactGroupForm extends PluginDmContactForm
{
  public function configure()
  {
    unset($this['contact_type']);
  }
}