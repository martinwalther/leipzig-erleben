<?php

/**
 * DmContact form base class.
 *
 * @method DmContact getObject() Returns the current form's model object
 *
 * @package    leipzig-erleben
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseDmContactForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'name'         => new sfWidgetFormInputText(),
      'email'        => new sfWidgetFormInputText(),
      'body'         => new sfWidgetFormTextarea(),
      'contact_type' => new sfWidgetFormChoice(array('choices' => array('simple' => 'simple', 'group' => 'group'))),
      'company'      => new sfWidgetFormInputText(),
      'first_name'   => new sfWidgetFormInputText(),
      'phone'        => new sfWidgetFormInputText(),
      'street'       => new sfWidgetFormInputText(),
      'zip'          => new sfWidgetFormInputText(),
      'city'         => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),

    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'         => new sfValidatorString(array('max_length' => 255)),
      'email'        => new sfValidatorString(array('max_length' => 255)),
      'body'         => new sfValidatorString(),
      'contact_type' => new sfValidatorChoice(array('choices' => array(0 => 'simple', 1 => 'group'), 'required' => false)),
      'company'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'first_name'   => new sfValidatorString(array('max_length' => 255)),
      'phone'        => new sfValidatorString(array('max_length' => 255)),
      'street'       => new sfValidatorString(array('max_length' => 255)),
      'zip'          => new sfValidatorString(array('max_length' => 255)),
      'city'         => new sfValidatorString(array('max_length' => 255)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('dm_contact[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'DmContact';
  }

}