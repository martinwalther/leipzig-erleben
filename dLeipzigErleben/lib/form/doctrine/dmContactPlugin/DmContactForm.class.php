<?php

/**
 * DmContact form.
 *
 * @package    leipzig-erleben
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
class DmContactForm extends PluginDmContactForm
{
  public function configure()
  {
    unset($this['company'], $this['contact_type']);
  }
}