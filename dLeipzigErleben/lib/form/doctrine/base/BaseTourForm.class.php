<?php

/**
 * Tour form base class.
 *
 * @method Tour getObject() Returns the current form's model object
 *
 * @package    leipzig-erleben
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseTourForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'is_walkabout'              => new sfWidgetFormInputCheckbox(),
      'is_excursion'              => new sfWidgetFormInputCheckbox(),
      'meeting_point'             => new sfWidgetFormInputText(),
      'duration'                  => new sfWidgetFormInputText(),
      'price'                     => new sfWidgetFormInputText(),
      'image'                     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'add_empty' => true)),
      'is_handicapped_accessible' => new sfWidgetFormInputCheckbox(),
      'position'                  => new sfWidgetFormInputText(),

    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_walkabout'              => new sfValidatorBoolean(array('required' => false)),
      'is_excursion'              => new sfValidatorBoolean(array('required' => false)),
      'meeting_point'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'duration'                  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'price'                     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'image'                     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'required' => false)),
      'is_handicapped_accessible' => new sfValidatorBoolean(array('required' => false)),
      'position'                  => new sfValidatorInteger(array('required' => false)),
    ));

    /*
     * Embed Media form for image
     */
    $this->embedForm('image_form', $this->createMediaFormForImage());
    unset($this['image']);

    if('embed' == sfConfig::get('dm_i18n_form'))
    {
      $this->embedI18n(sfConfig::get('dm_i18n_cultures'));
    }
    else
    {
      $this->mergeI18nForm();
    }

    $this->widgetSchema->setNameFormat('tour[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
  }

  public function getModelName()
  {
    return 'Tour';
  }

}