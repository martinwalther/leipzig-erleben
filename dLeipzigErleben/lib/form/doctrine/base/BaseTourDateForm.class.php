<?php

/**
 * TourDate form base class.
 *
 * @method TourDate getObject() Returns the current form's model object
 *
 * @package    leipzig-erleben
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseTourDateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'tour_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Tour'), 'add_empty' => false)),
      'date_day'   => new sfWidgetFormDmDate(),
      'date_time'  => new sfWidgetFormTime(),
      'is_active'  => new sfWidgetFormInputCheckbox(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),

    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'tour_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Tour'))),
      'date_day'   => new dmValidatorDate(),
      'date_time'  => new sfValidatorTime(),
      'is_active'  => new sfValidatorBoolean(array('required' => false)),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('tour_date[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'TourDate';
  }

}