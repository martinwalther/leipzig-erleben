<?php


class TourDateTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TourDate');
    }
}