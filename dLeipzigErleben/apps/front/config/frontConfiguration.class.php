<?php

require_once(dm::getDir().'/dmFrontPlugin/lib/config/dmFrontApplicationConfiguration.php');

class frontConfiguration extends dmFrontApplicationConfiguration
{
  public function configure()
  {
    $this->dispatcher->connect('dm.bread_crumb.filter_pages', array('breadcrumbListener', 'filterBreadcrumbPages'));
    $this->dispatcher->connect('dm_contact.saved', array($this, 'listenToContactSavedEvent'));
  }

  public function listenToContactSavedEvent(sfEvent $e)
  {
    $contact = $e['contact'];
    $additional_infos = $e['additional_infos'];

    $email_to = '';

    switch($contact['contact_type'])
    {
      case 'group':
        $email_to = 'enr.beck@googlemail.com';
        break;
      default:
      case 'simple':
        $email_to = 'enrico@enrico-beck.de';
        break;
    }

    $serviceContainer = $e->getSubject()->getServiceContainer();
    $serviceContainer->getService('mail')
        ->setTemplate('new_'.$contact['contact_type'].'_contact')
        ->addValues(array(
          'contact_company' => $contact['company'],
          'contact_first_name' => $contact['first_name'],
          'contact_name' => $contact['name'],
          'contact_email' => $contact['email'],
          'contact_phone' => $contact['phone'],
          'contact_street' => $contact['street'],
          'contact_zip' => $contact['zip'],
          'contact_city' => $contact['city'],
          'contact_body' => $contact['body'],
          'contact_created_at' => $contact['created_at'],
          'additional_infos' => $additional_infos
        ))
        ->send();
    /*$serviceContainer->getService('mail')
        ->setTemplate('new_'.$contact['contact_type'].'_contact_customer')
        ->addValues(array(
          'contact_company' => $contact['company'],
          'contact_name' => $contact['name'],
          'contact_email' => $contact['email'],
          'contact_phone' => $contact['phone'],
          'contact_street' => $contact['street'],
          'contact_zip' => $contact['zip'],
          'contact_city' => $contact['city'],
          'contact_body' => $contact['body'],
          'contact_created_at' => $contact['created_at'],
        ))
        ->send();*/

  }
}