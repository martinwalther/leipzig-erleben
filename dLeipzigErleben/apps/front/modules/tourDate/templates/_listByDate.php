<?php // Vars: $tourDatePager

use_helper('Date');

if (isset($year) && isset($month) && isset($day))
{
  echo _tag('h2.black', sprintf(__('Tours on %s'),format_date(sprintf('%s-%s-%s', $year, $month, $day), 'D')));
echo _tag('p.mb10', __('Click for tour information.').'<br/>');
  if ($tourDatePager->count())
  {
    //echo $tourDatePager->renderNavigationTop();

    echo _open('div.elements#results');

    

    $count = 0;
    foreach ($tourDatePager as $tourDate)
    {
      if ($tourDate->Tour->is_active)
      {
        $count++;
        echo _open('h4');

          echo '<a href="#">'
              .sprintf(__('%s: %s'), format_datetime($tourDate->getDateTime(), 't'), $tourDate->Tour)
              .'</a>';

        echo _close('h4');

        echo _open('div.element.display_none');

          echo dm_get_widget('tour', 'show', array('recordId' => $tourDate->Tour->id, 'date_day'=> $tourDate->date_day, 'date_time' => \date('H\hi',strtotime($tourDate->date_time))));
        echo _close('div');
      }
    }

    echo _close('div');

    //echo $tourDatePager->renderNavigationBottom();
  }

  if ($count == 0)
  {
    echo _tag('p', __('No tours found.'));
  }
}
else
{
  echo _tag('h2', __('Tours by date'));
  echo __('Please select a date in the calendar on the left!');
}