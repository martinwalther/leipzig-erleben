<?php // Vars: $tourDatePager

echo $tourDatePager->renderNavigationTop();

echo _open('ul.elements');

foreach ($tourDatePager as $tourDate)
{
  echo _open('li.element');

    echo $tourDate;

  echo _close('li');
}

echo _close('ul');

echo $tourDatePager->renderNavigationBottom();