<?php
/**
 * Tour Date components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 * 
 * 
 */
class tourDateComponents extends myFrontModuleComponents
{

  public function executeShow()
  {
    $query = $this->getShowQuery();
    
    $this->tourDate = $this->getRecord($query);
  }

  public function executeListByTour()
  {
    $query = $this->getListQuery();
    
    $this->tourDatePager = $this->getPager($query);
  }

  public function executeListByDate()
  {
    $query = $this->getListQuery('d')->leftJoin('d.Tour t');

    if ($this->getRequest()->hasParameter('date')){
      $date = $this->getRequest()->getParameter('date');
      list($year, $month, $day) = explode('-', $date);
      if (intval($year) && intval($month) && intval($day))
      {
        $query->andWhere('d.date_day = ?', sprintf('%s-%s-%s', $year, $month, $day))
              ->addOrderBy('t.position ASC');
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;
      }
    }
    $this->tourDatePager = $this->getPager($query);
  }

  public function executeListHandicap()
  {
    $query = $this->getListQuery();
    
    $this->tourDatePager = $this->getPager($query);
  }


}
