<?php
// Contact : Form
// Vars : $form

if($sf_user->hasFlash('contact_form_valid'))
{
  echo _tag('p.form_valid', __('Thank you, your contact request has been sent.'));
}
else
{

// open the form tag with a dm_contact_form css class
echo $form->open();

if ($form->hasErrors()):
  echo _tag('div.row.red', __('Es sind Fehler aufgetreten. Bitte korrigieren Sie alle rot markierten Felder.'));
endif;

echo _tag('div.row.clearfix', $form['company']->label()->field('.text')->error());

echo _tag('div.row.clearfix', $form['first_name']->label('First name*', $form['first_name']->hasError() ? '.has_error' : '')->field('.text')->error());

// write name label, field and error message
echo _tag('div.row.clearfix', $form['name']->label('Last name*', $form['name']->hasError() ? '.has_error' : '')->field('.text')->error());

// same with email
echo _tag('div.row.clearfix', $form['email']->label('Email*', $form['email']->hasError() ? '.has_error' : '')->field('.text')->error());

echo _tag('div.row.clearfix', $form['phone']->label('Phone*', $form['phone']->hasError() ? '.has_error' : '')->field('.text')->error());

echo _tag('div.row.clearfix', $form['street']->label('Street*', $form['phone']->hasError() ? '.has_error' : '')->field('.text')->error());

echo _tag('div.row.clearfix', $form['zip']->label('Zip*', $form['zip']->hasError() ? '.has_error' : '')->field('.shorttext')->error());

echo _tag('div.row.clearfix', $form['city']->label('City*', $form['city']->hasError() ? '.has_error' : '')->field('.text')->error());

echo _tag('div.notice_wrap.row', __('To ensure prompt processing please enter the name,<br  />date, and time of the tour you wish to book as well as<br />the required number of tickets into the field below.'));

echo _tag('div.row.clearfix', $form['body']->label('Your message*', $form['body']->hasError() ? '.has_error' : '')->field('.longtext')->error());

echo $form->renderHiddenFields();

echo _tag('div.notice_wrap.row', __('You will receive our offer of your wish tour as soon as possible.'));

// change the submit button text
echo _tag('div.submit_wrap.row.clearfix', $form->submit(__('Send')));

// close the form tag
echo $form->close();

}