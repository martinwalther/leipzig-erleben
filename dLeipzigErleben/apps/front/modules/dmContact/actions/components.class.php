<?php

//require_once realpath(dirname(__FILE__).'/..').'/lib/BasedmContactComponents.class.php';
require_once sfConfig::get('sf_plugins_dir').'/dmContactPlugin/modules/dmContact/lib/BasedmContactComponents.class.php';

/**
 * Contact components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 */
class dmContactComponents extends BasedmContactComponents
{
  public function executeFormGroup()
  {
    $this->form = $this->forms['DmContactGroup'];
  }
}
