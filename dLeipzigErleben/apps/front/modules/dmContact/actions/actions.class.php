<?php

//require_once realpath(dirname(__FILE__).'/..').'/lib/BasedmContactActions.class.php';
require_once sfConfig::get('sf_plugins_dir').'/dmContactPlugin/modules/dmContact/lib/BasedmContactActions.class.php';

/**
 * Contact actions
 */
class dmContactActions extends BasedmContactActions
{
  public function executeFormWidget(dmWebRequest $request)
  {
    $dmContact = new DmContact();
    $dmContact->setContactType('simple');
    $form = new DmContactForm($dmContact);

    if ($request->hasParameter($form->getName()))
    {
      $data = $request->getParameter($form->getName());

      if($form->isCaptchaEnabled())
      {
        $data = array_merge($data, array('captcha' => array(
          'recaptcha_challenge_field' => $request->getParameter('recaptcha_challenge_field'),
          'recaptcha_response_field'  => $request->getParameter('recaptcha_response_field'),
        )));
      }

      $form->bind($data, $request->getFiles($form->getName()));

      if ($form->isValid())
      {
        $form->save();

        $this->getUser()->setFlash('contact_form_valid', true);
        
        $lastPages = $this->getUser()->getAttribute('lastPages', array());
        $history = array();
        foreach ($lastPages as $timestamp => $page)
        {
	    $history[] = $timestamp . ' => ' . $page;
        }
        $additional_infos = 'Zuletzt besuchte Seiten:' . PHP_EOL . implode(PHP_EOL, $history);
        
        if ($request->hasParameter('tour'))
        {
	    $tourId = $request->getParameter('tour');
	    $tour = TourTable::getInstance()->find($tourId);
	    $additional_infos = 'Kontaktformular wurde aufgerufen über Tour "' . $tour->getName() . '"' . PHP_EOL . PHP_EOL . $additional_infos;
	    $this->getUser()->setFlash('show_tracking_code', 'bestellung');
        }

        $this->getService('dispatcher')->notify(new sfEvent($this, 'dm_contact.saved', array(
          'contact' => $form->getObject(),
          'additional_infos' => $additional_infos
        )));

        $this->redirectBack();
      }
    }

    $this->forms['DmContact'] = $form;
  }

  public function executeFormGroupWidget(dmWebRequest $request)
  {
    $dmContact = new DmContact();
    $dmContact->setContactType('group');
    $form = new DmContactGroupForm($dmContact);

    if ($request->hasParameter($form->getName()))
    {
      $data = $request->getParameter($form->getName());

      if($form->isCaptchaEnabled())
      {
        $data = array_merge($data, array('captcha' => array(
          'recaptcha_challenge_field' => $request->getParameter('recaptcha_challenge_field'),
          'recaptcha_response_field'  => $request->getParameter('recaptcha_response_field'),
        )));
      }

      $form->bind($data, $request->getFiles($form->getName()));

      if ($form->isValid())
      {
        $form->save();

        $this->getUser()->setFlash('contact_form_valid', true);

        $this->getService('dispatcher')->notify(new sfEvent($this, 'dm_contact.saved', array(
          'contact' => $form->getObject(),
          'additional_infos' => ''
        )));

        $this->redirectBack();
      }
    }

    $this->forms['DmContactGroup'] = $form;
  }
}
