<?php

require_once(dmOs::join(sfConfig::get('dm_front_dir'), 'modules/dmFront/lib/BasedmFrontActions.class.php'));

class dmFrontActions extends BasedmFrontActions
{
  public function executePage(dmWebRequest $request)
  {
    $this->page = $this->getPageFromRequest($request);
    $this->secure();
    
    if ($request->isMethod('get'))
    {
    
	$lastPages = $this->getUser()->getAttribute('lastPages', array());
    
	$lastPages[date('d.m.Y H:i:s')] = $request->getUri();
    
	$numLastPages = dmConfig::get('num_last_pages', 10);
    
	while (count($lastPages) > $numLastPages)
	{
	    array_shift($lastPages);
	}
    
	$this->getUser()->setAttribute('lastPages', $lastPages);
    
    }
    
    return $this->renderPage();
  }
}
