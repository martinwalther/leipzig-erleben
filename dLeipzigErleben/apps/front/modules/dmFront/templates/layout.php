<?php
/** @var dmFrontLayoutHelper */
$helper = $sf_context->get('layout_helper');

echo 
$helper->renderDoctype(),
$helper->renderHtmlTag(),

  "\n<head>\n",
  "\n<meta name=\"google-site-verification\" content=\"VspfUbsT0lZ6tfAVlSTQRtfLoQQqOc4bPnD3cclZC4Q\" />\n",
    $helper->renderHead(),
  "\n</head>\n",
  
  $helper->renderBodyTag(),
  
    $sf_content,
    
    $helper->renderEditBars(),
    
    $helper->renderJavascriptConfig(),
    $helper->renderJavascripts(),
    $helper->renderGoogleAnalytics(),
  
  "\n</body>\n",

"\n</html>";