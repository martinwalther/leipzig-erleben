<?php
/**
 * Partner components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 */
class partnerComponents extends myFrontModuleComponents
{

  public function executeList()
  {
    $query = $this->getListQuery();
    
    $this->partnerPager = $this->getPager($query);
  }


}
