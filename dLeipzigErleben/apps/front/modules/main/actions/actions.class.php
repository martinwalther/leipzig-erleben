<?php
/**
 * Main actions
 */
class mainActions extends myFrontModuleActions
{
  public function executeRedirectWidget(dmWebRequest $request)
  {
    $moduleAction = $this->getPage()->getModuleAction();
    $userCulture = $this->getUser()->getCulture();
//die($moduleAction);
    $newPage = '';
    switch($moduleAction)
    {
      case 'main/rundgange':
        $newPage = $userCulture == 'de' ? 'leipzig-kompakt' : 'leipzig-compact';
        break;
      case 'main/rundfahrten':
        $newPage = $userCulture == 'de' ? 'die-rundfahrt-leipzig-komplett-erleben' : 'leipzig-in-complete';
        break;
      case 'main/ausfluge';
        $newPage = $userCulture == 'de' ? 'das-gruene-leipzig' : 'day-trips';
        break;
      case 'main/besichtigungen';
        $newPage = $userCulture == 'de' ? 'grassi-museum-fuer-angewandte-kunst' : 'grassi-museum';
        break;
      case 'main/gruppen';
        $newPage = $userCulture == 'de' ? 'angebote' : 'offers';
        break;
      case 'main/stadtrallyes';
        $newPage = $userCulture == 'de' ? 'wissensrallye' : '';
        break;
      case 'main/kinderSchuler';
        $newPage = $userCulture == 'de' ? 'stadtrallyes' : 'tours-for-young-people-by-young-people';
        break;
    }
    if (strlen($newPage) && !sfConfig::get('sf_debug'))
    {
      $this->redirect($request->getUri() . '/' . $newPage);
    }
  }

  public function executeTicketsOnlineKaufen(dmWebRequest $request)
  {
    // todo
  }

}
