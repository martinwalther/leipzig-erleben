<?php

if ($sf_user->getCulture() == 'de')
{
  $switch_to = 'en';
  $text = 'English';
}
else
{
  $switch_to = 'de';
  $text = 'Deutsch';
}

echo _open('ul');
echo _tag('li',
  _link('@switchCulture')                 // link to the dmFront/selectCulture symfony action
  ->param('culture', $switch_to)          // the culture parameter
  ->param('page_id', $dm_page->id)        // by passing the current page id, we get redirected to this page after the culture changed
  ->text($text)                           // the link text
  ->set('#language_switcher.switch_to_'.$switch_to) // set css id and class
);
echo _close('ul');
