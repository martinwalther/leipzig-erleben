<?php
/**
 * Tour components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 * 
 * 
 * 
 */
class tourComponents extends myFrontModuleComponents
{

  public function executeList()
  {
    $query = $this->getListQuery();
    
    $this->tourPager = $this->getPager($query);
  }

  public function executeShow()
  {
    $query = $this->getShowQuery();
    
    $this->tour = $this->getRecord($query);
  }

  public function executeListByDate()
  {
    $query = $this->getListQuery();
    
    $this->tourPager = $this->getPager($query);
  }

  public function executeListHandicap()
  {
    $query = $this->getListQuery('t')->where('t.is_handicapped_accessible = ?', true);
    
    $this->tours = $query->execute();
  }

  public function executeBuyTickets()
  {
    $this->tour_slug = $this->getRequest()->getParameter('tour_slug');
    $this->tour_id = $this->getRequest()->getParameter('tour_id');
    $this->date_day = $this->getRequest()->getParameter('date_day');
    $this->date_time = $this->getRequest()->getParameter('date_time');
  }


}
