<?php // Vars: $tour
//var_dump($sf_user->getCulture());
//if ($tour->getIsActive()):
  echo _media($tour->getImage())->set('.fright.ml15.mb15');
  echo _tag('h3',$tour->getName());
  echo _tag('div.markdown', $tour->getDescription());
  echo $tour->getIsOnlineOrderable()
    ? _link('main/kontakt2?tour='.$tour->getId())->set('.bold.red.request-link')->text(__('Request/reserve tickets'))
    : _link('main/anfrage')->set('.bold.red.group-request-link')->text(__('Send request'));
  if (strlen($tour->getMeetingPoint())>0 && strlen($tour->getDuration())>0){
    echo _link('main/ticketsOnlineKaufen?tour_slug='.$tour->getMeetingPoint().'&tour_id='.$tour->getDuration().'&date_day='.$date_day.'&date_time='.$date_time)->set('.bold.red.citytixx-link')->text(__('Order tickets online'));
  }
  echo _tag('div.clr');
//endif;
