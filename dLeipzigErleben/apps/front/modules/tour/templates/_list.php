<?php // Vars: $tourPager

echo $tourPager->renderNavigationTop();

echo _open('ul.elements');

foreach ($tourPager as $tour)
{
  echo _open('li.element');

    echo _link($tour);

  echo _close('li');
}

echo _close('ul');

echo $tourPager->renderNavigationBottom();