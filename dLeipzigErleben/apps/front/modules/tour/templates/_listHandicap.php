<?php // Vars: $tourPager

echo _open('div.elements#results');

foreach ($tours as $tour):

if ($tour->getIsActive()):

echo _open('h4');
echo '<a href="#">'.$tour.'</a>';
echo _close('h4');

echo _open('div.element.display_none');

  echo dm_get_widget('tour', 'show', array('recordId' => $tour->id));

echo _close('div');

endif;

endforeach;

echo _close('div');

