<?php

class myMainMenu extends dmMenu
{
  /**
   * Recursively add children based on the page structure
   */
  public function addRecursiveChildren($depth = 1)
  {
    if($depth < 1 || !$this->getLink() instanceof dmFrontLinkTagPage || !$this->getLink()->getPage()->getNode()->hasChildren())
    {
      return $this;
    }

    $treeObject = dmDb::table('DmPage')->getTree();
    $treeObject->setBaseQuery(
      dmDb::table('DmPage')->createQuery('p')
      ->withI18n($this->user->getCulture(), null, 'p')
      ->select('p.*, pTranslation.*')
      ->andWhere('pTranslation.is_active = ?', true)  // show only child pages which are active
    );

    if($pageChildren = $this->getLink()->getPage()->getNode()->getChildren())
    {
      foreach($pageChildren as $childPage)
      {
        $this->addChild($childPage->get('name'), $childPage)->addRecursiveChildren($depth - 1);
      }
    }

    $treeObject->resetBaseQuery();

    return $this;
  }
}