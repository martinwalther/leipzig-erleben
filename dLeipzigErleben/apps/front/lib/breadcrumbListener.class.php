<?php

class breadcrumbListener {

    public static function filterBreadcrumbPages(sfEvent $event)
    {
        $pages = func_get_arg(1);

        array_shift($pages);
        
        return $pages;

    }

}