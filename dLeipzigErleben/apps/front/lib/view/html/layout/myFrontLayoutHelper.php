<?php

class myFrontLayoutHelper extends dmFrontLayoutHelper
{
  public function getJavascriptConfig()
  {
    $request = $this->getService('request');

    //print_r($request);

    return array_merge(parent::getJavascriptConfig(), array(
      'date' => $request->getParameter('date')
    ));
  }
}