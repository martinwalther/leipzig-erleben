<?php

/**
 * tour admin form
 *
 * @package    leipzig-erleben
 * @subpackage tour
 * @author     Your name here
 */
class TourAdminForm extends BaseTourForm
{
  public function configure()
  {
    parent::configure();
    
    $this->getValidator('image_form')->offsetSet('id', new sfValidatorDoctrineChoice(array('model' => 'DmMedia', 'required' => false)));
  }
}