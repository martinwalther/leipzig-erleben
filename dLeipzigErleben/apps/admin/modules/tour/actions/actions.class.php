<?php

require_once dirname(__FILE__).'/../lib/tourGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/tourGeneratorHelper.class.php';

/**
 * tour actions.
 *
 * @package    leipzig-erleben
 * @subpackage tour
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class tourActions extends autoTourActions
{
}
