<?php

require_once dirname(__FILE__).'/../lib/tourDateGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/tourDateGeneratorHelper.class.php';

/**
 * tourDate actions.
 *
 * @package    leipzig-erleben
 * @subpackage tourDate
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class tourDateActions extends autoTourDateActions
{
}
