<?php

require_once(dirname(__FILE__).'/../dLeipzigErleben/config/ProjectConfiguration.class.php');

$configuration = ProjectConfiguration::getApplicationConfiguration('admin', 'dev', true);

dm::createContext($configuration)->dispatch();